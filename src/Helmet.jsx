import React from 'react';
import { Helmet } from 'react-helmet';

function DynamicTitle(props) {

    return (
      <div>
            
    <Helmet>
          <title>{ props.titlePage}</title>
  </Helmet>
      </div>
  );
}

export default DynamicTitle;
